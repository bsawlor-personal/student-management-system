<?php
    require_once "../db.php";

    $sqlperson = "SELECT * FROM person";
    $sqlclass = "SELECT * FROM class";

    if(isset($_POST['submit'])){ // button name
        EnrollStudents();
        
    }

    function EnrollStudents(){
        // $result = $GLOBALS['conn']->query($GLOBALS['sql']);
        foreach ($_POST['classes'] as $classid )
            foreach ($_POST['students'] as $student)
            {
                $sql = "SELECT * FROM enrollment where ClassID='{$classid}' and PersonID='{$student}'";
                $result = $GLOBALS['conn']->query($sql);
                $sqlinsert = "Insert into enrollment (ClassID, PersonID) values('{$classid}','{$student}')";
                $sqlstudentid = "SELECT FirstName, LastName from person where Personid='{$student}'";
                $sqlclassid = "SELECT Number, Name from class where ID='{$classid}'";
                console_log($result);
                // $row_cnt = $result->num_rows;
                // $enrolledstudent = $result->fetch_assoc();

                if (!$result) {
                    $studentdetails = $GLOBALS['conn']->query($sqlstudentid);
                    $classdetails = $GLOBALS['conn']->query($sqlclassid);
                    $studentname = $studentdetails->fetch_assoc();
                    $classname = $classdetails->fetch_assoc();
                    console_log($studentname);
                    console_log($classname);
                    echo '<script type="text/javascript">alert("' . $studentname['FirstName'] . ' ' . $studentname['LastName'] . ' is already taking ' . $classname['Number'] . ': ' . $classname['Name'] . '.");</script>';
                } else {
                    $result = $GLOBALS['conn']->query($sqlinsert);
                }
            };
        
        echo "<meta http-equiv='refresh' content='0'>";
    }

    function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    //^ From for logging and troubleshooting
    // https://stackify.com/how-to-log-to-console-in-php/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../styles/normalize.css">

    <!-- Bootstrap Css -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">

    <!-- Script -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Management Home</title>
</head>
<body>

    <header>
        <h2>School Managment System</h2>
    </header>
    <nav id="nav_menu">
        <div class="table">
            <ul>
                <li><a href="/sms/" tabindex="1">Home</a></li>
                <li><a href="/sms/register/" tabindex="2">Student Registration</a></li>
                <li><a href="/sms/class-creation/" tabindex="3">Class Creation</a></li>
                <li><a href="/sms/class-administration/" tabindex="4" class="current">Class Administration</a></li>
                <li><a href="/sms/teacher-administration/" tabindex="5">Teacher Administration</a></li>
            </ul>
        </div>
    </nav>
    <main>
    <div>
        <h1>Class Administration</h1>
        <h3>Fill in the following information for the new Class:</h3>
    

        <form action="" method="post" id="class">
            <div>
                <label for="students">Select Students:</label>
                <select name="students[]" multiple size="20" method="post" tabindex="10" required id="selectStudents">
                    <?php
                        $result = $GLOBALS['conn']->query($GLOBALS['sqlperson']);
                        while ($resultrow = $result->fetch_assoc()) {
                            echo "<option value = " . $resultrow['Personid']. ">" . $resultrow['FirstName'] . " " . $resultrow['LastName'] . "</option>";                                     
                        } 
                    ?>
                </select>
            </div>

            <div>
                <label for="classes">Select Classes:</label>
                <select name="classes[]" multiple size="20" method="post" tabindex="11" required id="selectClasses">
                    <!-- I wish there was a maxsize that could be set for select lists, it would reduce the empty white 
                    space when they aren't as full -->
                    <?php
                        $result = $GLOBALS['conn']->query($GLOBALS['sqlclass']);
                        while ($resultrow = $result->fetch_assoc()) {
                            echo "<option value = " . $resultrow['ID']. ">" . $resultrow['Number'] . ": " . $resultrow['Name'] . "</option>";                                     
                        } 
                    ?>
                </select>
            </div>
            
            <input type="submit" name="submit" value="Register" id="submit" tabindex="12">

        </form>



    </div>
    </main>

    <footer>

        <?php include '../footer.php';?>
    </footer>
    
</body>
</html>