**Brock Sawlor**
**5558077**

## Setup

Site was created and tested using WAMP.  Folder should be located in the equivalent location when testing.  The sms.sql file was generated from a MySQL db, run or change accordingly to match what you use.

---

## Licence

This licence was chosen because it allows for usage, and also requires other works that use this code to provide the end source code available.

## Installation

Everything in the master folder needs to be copied to a folder that is inside the www folder, named something relevant.  The sms.sql needs to be run in the MySQL interface.

## Test - this now (merge as well)

This is a test to commit, then revert the commit.