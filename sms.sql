-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 30, 2021 at 06:35 PM
-- Server version: 8.0.21
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--
CREATE DATABASE [IF NOT EXISTS] sms;
-- --------------------------------------------------------
use sms;
--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Number` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TeacherID` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`ID`, `Number`, `TeacherID`, `Name`) VALUES
(1, 'PROG8020-1', 3, 'Programming Web Design & Dev'),
(2, 'PROG8080-1', 1, 'Programming: Database Mgmt');

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

DROP TABLE IF EXISTS `enrollment`;
CREATE TABLE IF NOT EXISTS `enrollment` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ClassID` int NOT NULL,
  `PersonID` int NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `enrollment`
--

INSERT INTO `enrollment` (`ID`, `ClassID`, `PersonID`) VALUES
(1, 1, 5),
(2, 2, 5),
(3, 1, 7),
(4, 1, 6),
(5, 2, 7),
(6, 2, 6),
(7, 1, 8),
(8, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `Personid` int NOT NULL AUTO_INCREMENT,
  `LastName` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `DOB` varchar(15) NOT NULL,
  `DOA` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Personid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`Personid`, `LastName`, `FirstName`, `Phone`, `Address`, `DOB`, `DOA`) VALUES
(8, 'Choice', 'Frank', '4568791325', '486 River Road', '18/07/1994', '08/10/2021'),
(7, 'test', 'another', '3756145789', '645 Queen st', '06/07/1992', '12/09/2021'),
(6, 'me', 'test', '4659875456', '897 queen st', '12/06/1993', '16/11/2021'),
(5, 'Sawlor', 'Brock', '5196458755', '123 king st', '25/06/1990', '15/10/2021'),
(9, 'Dewitt', 'Will', '7964684657', '396 University Ave', '09/02/1990', '24/09/2021'),
(10, 'One', 'No', '4657891325', '257 What Way', '03/06/1999', '19/11/2021');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE IF NOT EXISTS `teacher` (
  `Teacherid` int NOT NULL AUTO_INCREMENT,
  `LastName` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  PRIMARY KEY (`Teacherid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`Teacherid`, `LastName`, `FirstName`, `Phone`) VALUES
(1, 'Olubadejo', 'Bami', '5196845754'),
(2, 'Sarkut', 'Oz', '4165468975'),
(3, 'Atique', 'Noman', '5191584685'),
(4, 'Kozak', 'Rick', '5194685745'),
(5, 'Tanuan', 'Meyer', '5194863245');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
