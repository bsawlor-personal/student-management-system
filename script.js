function updateTable() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("enrolled").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "enrolled-table.txt", true);
    xhttp.send();
}

function updateAllTable() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("enrolled").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "all-enrolled-table.txt", true);
    xhttp.send();
}