<?php
    require_once "db.php";

    $sqlclass = "SELECT * FROM class";

    $sqlall = "SELECT P.Personid as `Student ID`, P.FirstName as `Student First Name`, P.LastName as `Student Last Name`, C.Number as `Section Name`, C.Name as `Class Name`, T.FirstName as `Teacher First Name`, T.LastName as `Teacher Last Name` FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid";
    //$sqlsection = "SELECT * FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid and C.Number = '{$section}''";

    // Originally I was going to rebuild the table, recapturing the data from the db based on the selection and replacing the table with the code in either txt file, depending on
    // the selection, first through js file, then the code immediately below this. 
    
    // Ass none of those were working, and the table wasn't being updated, I then thought about filtering the existing table
    // That is the current code in the Selection function in the script section.


    // I also tried to use the jquery code to have the alternating row colours via css update upon the filtering being done.  
    // That isn't working for some reason.


    // if(isset($_POST['classes'])){ // button name
    //     Selection();
    // }

    // function Selection(){
    //     //$selection = $_POST['classes'];
    //     console_log($_POST['classes']);

    //     if ($_POST['classes'] == "all") {
    //         $sqlall = "SELECT P.Personid as `Student ID`, P.FirstName as `Student First Name`, P.LastName as `Student Last Name`, C.Number as `Section Name`, C.Name as `Class Name`, T.FirstName as `Teacher First Name`, T.LastName as `Teacher Last Name` FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid";
    //         $GLOBALS['sqlall'] = "SELECT P.Personid as `Student ID`, P.FirstName as `Student First Name`, P.LastName as `Student Last Name`, C.Number as `Section Name`, C.Name as `Class Name`, T.FirstName as `Teacher First Name`, T.LastName as `Teacher Last Name` FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid";

    //         ? >
    //         <script type="text/javascript">
    //             function updateAllTable() {
    //                 var xhttp = new XMLHttpRequest();
    //                 xhttp.onreadystatechange = function() {
    //                     if (this.readyState == 4 && this.status == 200) {
    //                         document.getElementById("enrolled").innerHTML = this.responseText;
    //                     }
    //                 };
    //                 xhttp.open("GET", "all-enrolled-table.txt", true);
    //                 xhttp.send();
    //             }
    //         </script>
    //         <?php
    //     } else {
    //         $sqlclass = "SELECT Number FROM class where ID = '{$_POST['classes']}'";
    //         $section = $GLOBALS['conn']->query($sqlclass);
    //         $sqlsection = "SELECT P.Personid as `Student ID`, P.FirstName as `Student First Name`, P.LastName as `Student Last Name`, C.Number as `Section Name`, C.Name as `Class Name`, T.FirstName as `Teacher First Name`, T.LastName as `Teacher Last Name` FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid and C.Number = '{$section}'";
    //         $GLOBALS['sqlsection'] = "SELECT P.Personid as `Student ID`, P.FirstName as `Student First Name`, P.LastName as `Student Last Name`, C.Number as `Section Name`, C.Name as `Class Name`, T.FirstName as `Teacher First Name`, T.LastName as `Teacher Last Name` FROM person P, class C, enrollment E, teacher T where P.Personid = E.PersonID and E.ClassID = C.ID and C.TeacherID = T.Teacherid and C.Number = '{$section}'";
    //         ? >
    //         <script type="text/javascript">
    //             function updateTable() {
    //                 var xhttp = new XMLHttpRequest();
    //                 xhttp.onreadystatechange = function() {
    //                     if (this.readyState == 4 && this.status == 200) {
    //                         document.getElementById("enrolled").innerHTML = this.responseText;
    //                     }
    //                 };
    //                 xhttp.open("GET", "enrolled-table.txt", true);
    //                 xhttp.send();
    //             }
    //         </script>
    //         <?php
    //     }
    //     // echo "<meta http-equiv='refresh' content='0'>";
    // }

    function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
        ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="styles/normalize.css">

        <!-- Bootstrap Css -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

        <!-- jQuery UI -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

        <link rel="stylesheet" href="styles/main.css">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

        <!-- Script -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

        <!-- <script src="script.js"></script>  -->

        <script>
            function Selection() {
                var inputValue = document.getElementById("enrolledStudents");
                var input = inputValue.value;
                var table, tr, td, i, txtValue;
                table = document.getElementById("enrolled");
                tr = table.getElementsByTagName("tr");
                console.log("value in js "+input);
                if(input != "all"){
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[3];
                        if (td) {
                            txtValue = td.textContent || td.innerText;
                            if (txtValue.toUpperCase().indexOf(input) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }       
                    }
                } else {
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[3];
                        tr[i].style.display = "";
                    }
                }
            }
            jQuery(function($) {
                var $table = $('#enrolled')
                , $style = $('#styles')
                , $select = $('#enrolledStudents')
                ;
                $table.val($style.html());
                $select.change(function() {
                    $style.html($table.val());
                    return false;
                });
            });

        </script>

        <style type="text/css" id="styles">
            table tr:nth-child(even) {
                background-color: #eee;
            }

            table tr:nth-child(odd) {
                background-color: #fff;
            }
        </style>    
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>School Management Home</title>
    </head>
    <body>
        <header>
            <h2>School Managment System</h2>
        </header>
        <nav id="nav_menu">
            <div class="table">
                <ul>
                    <li><a href="/sms/" class="current" tabindex="1">Home</a></li>
                    <li><a href="/sms/register/" tabindex="2">Student Registration</a></li>
                    <li><a href="/sms/class-creation/" tabindex="3">Class Creation</a></li>
                    <li><a href="/sms/class-administration/" tabindex="4">Class Administration</a></li>
                    <li><a href="/sms/teacher-administration/" tabindex="5">Teacher Administration</a></li>
                </ul>
            </div>
        </nav>
        <main id="home">
            <div>
                <h1>Browse Enrollment</h1>
                <h3>Select Class Section or all enrolled students:</h3>
                <div>
                    <form>
                        <label for="classes">Select Class:</label>
                        <select name="classes[]" size="1" onchange="Selection()" id="enrolledStudents" tabindex="10" method="post">
                            <option value="all" >All Students</option>
                            <?php
                                $result = $GLOBALS['conn']->query($GLOBALS['sqlclass']);
                                while ($resultrow = $result->fetch_assoc()) {
                                    echo "<option value = " . $resultrow['Number']. ">" . $resultrow['Number'] . ": " . $resultrow['Name'] . "</option>";                                     
                                } 
                            ?>
                        </select>
                    </form>
                </div>
                <!-- <button class="" onclick="" name="enrolled" tabindex="11">Enrolled Students</button> -->

                <table tabindex="12" method="post" id="enrolled">
                    <!-- <thead> -->
                        <tr id="header">
                            <th id="first">Student ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Section Name</th>
                            <th>Class Name</th>
                            <th>Teachers First Name</th>
                            <th id="last">Teachers First Name</th>
                        </tr>
                    <!-- </thead>
                    <tbody> -->
                        <?php
                        $result = $GLOBALS['conn']->query($GLOBALS['sqlall']);
                        console_log($sqlall);
                        console_log($result);

                        // echo $result;
                        while ($resultrow = $result->fetch_assoc()) {?>
                            <tr>
                            <td><?php echo $resultrow['Student ID'];?></td>
                            <td><?php echo $resultrow['Student First Name'];?></td>
                            <td><?php echo $resultrow['Student Last Name'];?></td>
                            <td><?php echo $resultrow['Section Name'];?></td>
                            <td><?php echo $resultrow['Class Name'];?></td>
                            <td><?php echo $resultrow['Teacher First Name'];?></td>
                            <td><?php echo $resultrow['Teacher Last Name'];?></td>
                            </tr>
                        <?php  } ?>
                    <!-- </tbody> -->
                </table>
            </div>
        </main>

        <footer>
            <?php include 'footer.php';?>

        </footer>
        
    </body>
</html>