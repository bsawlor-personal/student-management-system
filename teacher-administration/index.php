<?php
    require_once "../db.php";

    $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
    $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    // $address = isset($_POST['address']) ? $_POST['address'] : '';
    // $dob = isset($_POST['dob']) ? $_POST['dob'] : '';
    // $doa = isset($_POST['doa']) ? $_POST['doa'] : '';

    $sql = "SELECT LastName, FirstName, Phone FROM teacher where LastName='{$lastname}' and FirstName='{$firstname}' and Phone='{$phone}'";
    $sqlinsert = "Insert into teacher (LastName, FirstName, Phone) values('{$lastname}','{$firstname}','{$phone}')";

    if(isset($_POST['submit'])){ // button name
        TeacherExists();
    }

    function TeacherExists(){
        $result = $GLOBALS['conn']->query($GLOBALS['sql']);

        if ($result->num_rows > 0) {
            echo "<script type='text/javascript'>alert('This teacher is already in the system.');</script>";

        } else {
            $result = $GLOBALS['conn']->query($GLOBALS['sqlinsert']);
        }
        echo "<meta http-equiv='refresh' content='0'>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../styles/normalize.css">

    <!-- Bootstrap Css -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">

    <!-- Script -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Management Home</title>
</head>
<body>

    <header>
        <h2>School Managment System</h2>
    </header>
    <nav id="nav_menu">
        <div class="table">
            <ul>
                <li><a href="/sms/"  tabindex="1">Home</a></li>
                <li><a href="/sms/register/" tabindex="2">Student Registration</a></li>
                <li><a href="/sms/class-creation/" tabindex="3">Class Creation</a></li>
                <li><a href="/sms/class-administration/" tabindex="4">Class Administration</a></li>
                <li><a href="/sms/teacher-administration/" class="current" tabindex="5">Teacher Administration</a></li>
            </ul>
        </div>
    </nav>
    <main>
        <div>
            <h1>Teacher Registration</h1>
            <h3>Fill in the following information for new teachers:</h3>
            <form action="" method="post">
                <div>
                <div>
                        <label for="firstname">First Name:</label>
                        <input name="firstname" id="firstname" required autofocus tabindex="10" method="post">
                    </div>

                    <div>
                        <label for="lastname">Last Name:</label>
                        <input name="lastname" id="lastname" required tabindex="11">
                    </div>

                    <div>   
                        <label for="phone">Phone Number:</label>
                        <input name="phone" id="phone" required tabindex="12" pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$">
                    </div>

                    <!-- <div>
                        <label for="address">Current Address:</label>
                        <input name="address" id="address" required tabindex="13" >
                    </div>

                    <div>   
                        <label for="dob">Date Of Birth:</label>
                        <input name="dob" id="dob" required tabindex="14" onfocus="this.value=''" placeholder="mm/dd/yyyy" >
                    </div>

                    <div>
                        <label for="doa">Date of Admission:</label>
                        <input name="doa" id="doa" required tabindex="15" onfocus="this.value=''" placeholder="mm/dd/yyyy" >
                    </div> -->
                </div>
                <input type="submit" name="submit" value="Register" id="submit" tabindex="16">
            </form>
        </div>
    </main>

    <footer>

        <?php include '../footer.php';?>
    </footer>
    
</body>
</html>