<?php
    require_once "../db.php";

    $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
    $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    $address = isset($_POST['address']) ? $_POST['address'] : '';
    $dob = isset($_POST['dob']) ? $_POST['dob'] : '';
    $doa = isset($_POST['doa']) ? $_POST['doa'] : '';

    $sql = "SELECT LastName, FirstName, Phone, Address, DOB, DOA FROM person where LastName='{$lastname}' and FirstName='{$firstname}' and Phone='{$phone}' and Address='{$address}' and DOB='{$dob}' and DOA='{$doa}'";
    $sqlinsert = "Insert into person (LastName, FirstName, Phone, Address, DOB, DOA) values('{$lastname}','{$firstname}','{$phone}','{$address}','{$dob}','{$doa}')";

    if(isset($_POST['submit'])){ // button name
        PersonExists();
    }

    function PersonExists(){
        $result = $GLOBALS['conn']->query($GLOBALS['sql']);

        if ($result->num_rows > 0) {
            echo "<script type='text/javascript'>alert('This person is already in the system.');</script>";

        } else {
            $result = $GLOBALS['conn']->query($GLOBALS['sqlinsert']);
        }
        // echo "<meta http-equiv='refresh' content='0'>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../styles/normalize.css">

    <!-- Bootstrap Css -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">

    <!-- Script -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Management Home</title>
</head>
<body>

    <header>
        <h2>School Managment System</h2>
    </header>
    <nav id="nav_menu">
        <div class="table">
            <ul>
                <li><a href="/sms/"  tabindex="1">Home</a></li>
                <li><a href="/sms/register/" class="current" tabindex="2">Student Registration</a></li>
                <li><a href="/sms/class-creation/" tabindex="3">Class Creation</a></li>
                <li><a href="/sms/class-administration/" tabindex="4">Class Administration</a></li>
                <li><a href="/sms/teacher-administration/" tabindex="5">Teacher Administration</a></li>
            </ul>
        </div>
    </nav>
    <main>
        <div>
            <h1>Student Registration</h1>
            <h3>Fill in the following information for new students:</h3>
            <form action="" method="post">
                <div>
                    <div>
                        <label for="firstname">First Name:</label>
                        <input name="firstname" id="firstname" required autofocus tabindex="10" method="post">
                    </div>

                    <div>
                        <label for="lastname">Last Name:</label>
                        <input name="lastname" id="lastname" required tabindex="11" >
                    </div>

                    <div>   
                        <label for="phone">Phone Number:</label>
                        <input name="phone" id="phone" required tabindex="12" pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$">
                    </div>

                    <div>
                        <label for="address">Current Address:</label>
                        <input name="address" id="address" required tabindex="13" >
                    </div>

                    <div>   
                        <label for="dob">Date Of Birth:</label>
                        <input name="dob" id="dob" required tabindex="14" onfocus="this.value=''" placeholder="dd/mmm/yyyy or dd/mm/yyyy" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                    </div>

                    <div>
                        <label for="doa">Date of Admission:</label>
                        <input name="doa" id="doa" required tabindex="15" onfocus="this.value=''" placeholder="dd/mmm/yyyy or dd/mm/yyyy" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                    </div>
                </div>
                <input type="submit" name="submit" value="Register" id="submit" tabindex="16">
            </form>
        </div>
    </main>

    <footer>

        <?php include '../footer.php';?>
    </footer>
    
</body>
</html>