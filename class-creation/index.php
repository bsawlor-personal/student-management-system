<?php
    require_once "../db.php";

    $sqlteach = "SELECT * FROM teacher";

    $classname = isset($_POST['classname']) ? $_POST['classname'] : '';
    $sectionname = isset($_POST['sectionname']) ? $_POST['sectionname'] : '';
    // $teacherfn = isset($_POST['teacherfn']) ? $_POST['teacherfn'] : '';
    // $teacherln = isset($_POST['teacherln']) ? $_POST['teacherln'] : '';

    $sqlclass = "SELECT Name, Number FROM class where Name='{$classname}' and Number='{$sectionname}'";
    //$sql = "SELECT LastName, FirstName, FROM teacher where LastName='{$teacherln}' and FirstName='{$teacherfn}'";
    // $sqlinsert = "Insert into class (Number, TeacherID, Name) values('{$sectionname}','{$teacherid}','{$classname}')";
    //$sqlteacherid = "SELECT LastName, FirstName, FROM teacher where LastName='{$teacherln}' and FirstName='{$teacherfn}'";

    if(isset($_POST['submit'])){ // button name
        ClassExists();
    }

    function ClassExists(){
        $result = $GLOBALS['conn']->query($GLOBALS['sqlclass']);
        console_log($result);

        $row_cnt = $result->num_rows;
        // printf($row_cnt);

        console_log($row_cnt);

        $teacherid = $_POST['teachers'];

        if ($row_cnt == 0) {
            $sqlinsert = "Insert into class (Number, TeacherID, Name) values('{$GLOBALS['sectionname']}', '{$teacherid}', '{$GLOBALS['classname']}')";
            //console_log($sqlinsert);
            $result = $GLOBALS['conn']->query($sqlinsert);
        } else if ($row_cnt > 0){
            echo "<script type='text/javascript'>alert('This class is already in the system.');</script>";
        }

        console_log($GLOBALS['sectionname']);
        console_log($teacherid);
        console_log($GLOBALS['classname']);
        console_log($result);

        // echo "<meta http-equiv='refresh' content='0'>";
    }

    function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    //^ From for logging and troubleshooting
    // https://stackify.com/how-to-log-to-console-in-php/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../styles/normalize.css">
    
    <!-- Bootstrap Css -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    
    <link rel="stylesheet" href="../styles/main.css">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">

    <!-- Script -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Management Home</title>
</head>
<body>

    <header>
        <h2>School Managment System</h2>
    </header>
    <nav id="nav_menu">
        <div class="table">
            <ul>
                <li><a href="/sms/" tabindex="1">Home</a></li>
                <li><a href="/sms/register/" tabindex="2">Student Registration</a></li>
                <li><a href="/sms/class-creation/" class="current" tabindex="3">Class Creation</a></li>
                <li><a href="/sms/class-administration/" tabindex="4">Class Administration</a></li>
                <li><a href="/sms/teacher-administration/" tabindex="5">Teacher Administration</a></li>
            </ul>
        </div>
    </nav>
    <main>
        <div>
            <h1>Class Registration</h1>
            <h3>Fill in the following information for the new Class:</h3>
            <form action="" method="post" >
                <div>
                    <div>
                        <label for="classname">Class Name:</label>
                        <input name="classname" id="classname" required autofocus tabindex="10" method="post">
                    </div>

                    <div>
                        <label for="sectionname">Section Name:</label>
                        <input name="sectionname" id="sectionname" required tabindex="11" method="post">
                    </div>

                    <div>
                        <label for="teachers">Select Teacher:</label>
                        <select name="teachers" size="1" method="post" required tabindex="12" id="selectedTeachers">
                            <?php
                                $result = $GLOBALS['conn']->query($GLOBALS['sqlteach']);
                                while ($resultrow = $result->fetch_assoc()) {
                                    echo "<option value = " . $resultrow['Teacherid']. ">" . $resultrow['FirstName'] . " " . $resultrow['LastName'] . "</option>";                                     
                                } ?>
                            
                        </select>
                    </div>
                </div>
                <input type="submit" name="submit" value="Create Class" id="submit" tabindex="13">
            </form>
        </div>
    </main>

    <footer>

        <?php include '../footer.php';?>
    </footer>
</body>
</html>